/*
Autor: Fatima Azucena MC
Fecha: 24_11_2020
Correo: fatimaazucenamartinez274gmail.com
 */
package com.mycompany.calculadora.registropersona;

public class Llamada {//Inicio clase principal llamada
    public static void main(String [] args){//Inicio método principal
        //Creacion del objeto para poder llamar a la clase TablaRegistro
        TablaDeRegistro objTablaDeRegistro = new TablaDeRegistro();
        objTablaDeRegistro.tabla();
    }//Fin método principal
}//Fin clase principal
